<?php

namespace UxiT\PostsClient;

class PostsClientProvider
{
    /** @var string[] */
    public static $apis = ['\UxiT\PostsClient\Api\VotesApi', '\UxiT\PostsClient\Api\PostsApi'];

    /** @var string[] */
    public static $dtos = [
        '\UxiT\PostsClient\Dto\RequestBodyOffsetPagination',
        '\UxiT\PostsClient\Dto\CreateVoteRequest',
        '\UxiT\PostsClient\Dto\EmptyDataResponse',
        '\UxiT\PostsClient\Dto\SearchPostResponseMeta',
        '\UxiT\PostsClient\Dto\PatchPostRequest',
        '\UxiT\PostsClient\Dto\VoteReadOnlyProperties',
        '\UxiT\PostsClient\Dto\PostResponse',
        '\UxiT\PostsClient\Dto\ResponseBodyPagination',
        '\UxiT\PostsClient\Dto\ModelInterface',
        '\UxiT\PostsClient\Dto\PostReadOnlyProperties',
        '\UxiT\PostsClient\Dto\PaginationTypeOffsetEnum',
        '\UxiT\PostsClient\Dto\VoteFillableProperties',
        '\UxiT\PostsClient\Dto\SearchVoteRequest',
        '\UxiT\PostsClient\Dto\Vote',
        '\UxiT\PostsClient\Dto\Post',
        '\UxiT\PostsClient\Dto\SearchPostsRequest',
        '\UxiT\PostsClient\Dto\CreatePostRequest',
        '\UxiT\PostsClient\Dto\PaginationTypeEnum',
        '\UxiT\PostsClient\Dto\ResponseBodyCursorPagination',
        '\UxiT\PostsClient\Dto\ResponseBodyOffsetPagination',
        '\UxiT\PostsClient\Dto\PaginationTypeCursorEnum',
        '\UxiT\PostsClient\Dto\Error',
        '\UxiT\PostsClient\Dto\SearchVoteResponse',
        '\UxiT\PostsClient\Dto\SearchPostsFilter',
        '\UxiT\PostsClient\Dto\ErrorResponse',
        '\UxiT\PostsClient\Dto\RequestBodyCursorPagination',
        '\UxiT\PostsClient\Dto\SearchVoteFilter',
        '\UxiT\PostsClient\Dto\SearchPostResponse',
        '\UxiT\PostsClient\Dto\PostFillableProperties',
        '\UxiT\PostsClient\Dto\RequestBodyPagination',
        '\UxiT\PostsClient\Dto\VoteResponse',
    ];

    /** @var string */
    public static $configuration = '\UxiT\PostsClient\Configuration';
}
