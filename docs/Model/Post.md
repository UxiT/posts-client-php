# # Post

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Числовое значение id Поста | [optional] 
**votes** | **int** | Числовое значение рейтинга поста (Все голоса за - голоса против) | [optional] 
**user_id** | **int** | Числовое значение id автора поста | [optional] 
**title** | **string** | Заголовок поста | [optional] 
**text** | **string** | Текст поста | [optional] 
**image_url** | **string** | Ссылка на обложку поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


