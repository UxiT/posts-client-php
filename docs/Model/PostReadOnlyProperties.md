# # PostReadOnlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Числовое значение id Поста | [optional] 
**votes** | **int** | Числовое значение рейтинга поста (Все голоса за - голоса против) | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


