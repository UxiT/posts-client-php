# # VoteFillableProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_id** | **int** | Id пользователя, который оставил голос | [optional] 
**post_id** | **int** | Id поста, к которому оставили голос | [optional] 
**vote** | **bool** | true - голос за повышение рейтинга, false - голос за понижение рейтинга | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


