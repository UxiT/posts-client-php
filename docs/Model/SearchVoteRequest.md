# # SearchVoteRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sort** | **string[]** |  | [optional] 
**filter** | [**\UxiT\PostsClient\Dto\SearchVoteFilter**](SearchVoteFilter.md) |  | [optional] 
**include** | **string[]** |  | [optional] 
**pagination** | [**\UxiT\PostsClient\Dto\RequestBodyPagination**](RequestBodyPagination.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


