# # SearchVoteResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\UxiT\PostsClient\Dto\Vote[]**](Vote.md) |  | 
**meta** | [**\UxiT\PostsClient\Dto\SearchPostResponseMeta**](SearchPostResponseMeta.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


