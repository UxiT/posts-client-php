# # SearchPostsFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | id поста | [optional] 
**user_id** | **int** | id пользователя (автора поста) | [optional] 
**votes** | **int** | Рейтинг поста | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


