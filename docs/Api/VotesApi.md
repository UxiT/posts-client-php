# UxiT\PostsClient\VotesApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createVote**](VotesApi.md#createVote) | **POST** /posts/votes | Создаёт запись о голосе к посту
[**deleteVote**](VotesApi.md#deleteVote) | **DELETE** /posts/votes/{id} | Удаление объекта типа Vote
[**getVote**](VotesApi.md#getVote) | **GET** /posts/votes/{id} | Получение объекта типа Vote
[**patchVote**](VotesApi.md#patchVote) | **PATCH** /posts/votes/{id} | Патчинг записи о голосе пользователя к посту
[**searchVote**](VotesApi.md#searchVote) | **POST** /posts/votes:search | Поиск записи (записей) о голосах к посту



## createVote

> \UxiT\PostsClient\Dto\VoteResponse createVote($create_vote_request)

Создаёт запись о голосе к посту

Создаёт запись о голосе пользователя к посту

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new UxiT\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$create_vote_request = new \UxiT\PostsClient\Dto\CreateVoteRequest(); // \UxiT\PostsClient\Dto\CreateVoteRequest | 

try {
    $result = $apiInstance->createVote($create_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->createVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **create_vote_request** | [**\UxiT\PostsClient\Dto\CreateVoteRequest**](../Model/CreateVoteRequest.md)|  |

### Return type

[**\UxiT\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## deleteVote

> \UxiT\PostsClient\Dto\EmptyDataResponse deleteVote($id)

Удаление объекта типа Vote

Удаление объкта типа Vote с указанным id (Важно! удаление не всех записей о голосах к посту, а только конкретной записи с голосом за/против)

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new UxiT\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | id записи о голосе пользователя

try {
    $result = $apiInstance->deleteVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->deleteVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id записи о голосе пользователя |

### Return type

[**\UxiT\PostsClient\Dto\EmptyDataResponse**](../Model/EmptyDataResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## getVote

> \UxiT\PostsClient\Dto\VoteResponse getVote($id)

Получение объекта типа Vote

Получение объекта типа Vote

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new UxiT\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | id записи о голосе пользователя

try {
    $result = $apiInstance->getVote($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->getVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id записи о голосе пользователя |

### Return type

[**\UxiT\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## patchVote

> \UxiT\PostsClient\Dto\VoteResponse patchVote($id, $body)

Патчинг записи о голосе пользователя к посту

Патичинг записи о голосе пользователя к посту с указанным id

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new UxiT\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$id = 1; // int | id записи о голосе пользователя
$body = new \stdClass; // object | 

try {
    $result = $apiInstance->patchVote($id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->patchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| id записи о голосе пользователя |
 **body** | **object**|  | [optional]

### Return type

[**\UxiT\PostsClient\Dto\VoteResponse**](../Model/VoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)


## searchVote

> \UxiT\PostsClient\Dto\SearchVoteResponse searchVote($search_vote_request)

Поиск записи (записей) о голосах к посту

Посик объекта(-ов ) типа Vote, по параметрам, указанным в теле запроса

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new UxiT\PostsClient\Api\VotesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$search_vote_request = new \UxiT\PostsClient\Dto\SearchVoteRequest(); // \UxiT\PostsClient\Dto\SearchVoteRequest | 

try {
    $result = $apiInstance->searchVote($search_vote_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VotesApi->searchVote: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **search_vote_request** | [**\UxiT\PostsClient\Dto\SearchVoteRequest**](../Model/SearchVoteRequest.md)|  | [optional]

### Return type

[**\UxiT\PostsClient\Dto\SearchVoteResponse**](../Model/SearchVoteResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

