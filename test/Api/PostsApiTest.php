<?php
/**
 * PostsApiTest
 * PHP version 5
 *
 * @category Class
 * @package  UxiT\PostsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * task-85142
 *
 * RESTAPI task-85142 project
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace UxiT\PostsClient;

use \UxiT\PostsClient\Configuration;
use \UxiT\PostsClient\ApiException;
use \UxiT\PostsClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * PostsApiTest Class Doc Comment
 *
 * @category Class
 * @package  UxiT\PostsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class PostsApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createPost
     *
     * Создание объекта типа Post.
     *
     */
    public function testCreatePost()
    {
    }

    /**
     * Test case for deletePost
     *
     * Удаление объекта типа Post с указанным id.
     *
     */
    public function testDeletePost()
    {
    }

    /**
     * Test case for getPost
     *
     * Получение объекта типа Post.
     *
     */
    public function testGetPost()
    {
    }

    /**
     * Test case for patchPost
     *
     * Патчинг поста с указанным id.
     *
     */
    public function testPatchPost()
    {
    }

    /**
     * Test case for searchPost
     *
     * Поиск поста (постов).
     *
     */
    public function testSearchPost()
    {
    }
}
