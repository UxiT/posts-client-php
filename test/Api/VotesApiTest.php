<?php
/**
 * VotesApiTest
 * PHP version 5
 *
 * @category Class
 * @package  UxiT\PostsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * task-85142
 *
 * RESTAPI task-85142 project
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: mail@greensight.ru
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 4.3.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace UxiT\PostsClient;

use \UxiT\PostsClient\Configuration;
use \UxiT\PostsClient\ApiException;
use \UxiT\PostsClient\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * VotesApiTest Class Doc Comment
 *
 * @category Class
 * @package  UxiT\PostsClient
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class VotesApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for createVote
     *
     * Создаёт запись о голосе к посту.
     *
     */
    public function testCreateVote()
    {
    }

    /**
     * Test case for deleteVote
     *
     * Удаление объекта типа Vote.
     *
     */
    public function testDeleteVote()
    {
    }

    /**
     * Test case for getVote
     *
     * Получение объекта типа Vote.
     *
     */
    public function testGetVote()
    {
    }

    /**
     * Test case for patchVote
     *
     * Патчинг записи о голосе пользователя к посту.
     *
     */
    public function testPatchVote()
    {
    }

    /**
     * Test case for searchVote
     *
     * Поиск записи (записей) о голосах к посту.
     *
     */
    public function testSearchVote()
    {
    }
}
